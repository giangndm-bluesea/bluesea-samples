Bluesea Streaming JS-SDK

# Install steps

Include bluesea.sdk.cjs.js or bluesea.sdk.esm.js lib to web. With typescript, can include bluesea.sdk.d.ts for better.

# Bluesea Model

In Bluesea we have: LocalStream, RemoteStream, Sender(Producer), Receiver(Consumer), Render.

To make application with Bluesea, we simple create pipes between each parts like:

- Peer1: LocalStream => Sender => Server
- Peer2: Server => Remove Stream => Receiver => VideoElementRender

# APIs

## 1. Init session

``` ts
let session = createBlueSeaSession(servers: String | String[], {
        room_id: String,
        peer_id: String,
        token: String,
        codecs?: String[], //'OPUS', 'VP8', 'VP9', 'H264'
        senders: [
            { stream: MediaStream, name: String, kind: 'audio' },
            { stream: MediaStream, name: String, kind: 'video', simulcast: boolean, prefer_codecs?: String[] }
            ...
        ],
        receivers: { 
            audio: number,
            video: number
        }
});
```

With:

- servers: single server or list servers. In case of list servers, system will ping server with HTTP-Healthcheck for selecting fastest server
- peer_id: uuid of user
- token: user token, in dev server this can be null, in production using createSessionApi from server side
- senders can be empty
- receivers: Pre defined number of incoming audio and video streams, this reduce time to connect to a remote stream

## 2. Session's properties and events

Methods:
``` ts
connect(): Promise<void>;
disconnect(): Promise<void>;
ready(): Promise<unknown>; //Waiting ready
createPublisher(kind: StreamKinds, name: string, simulcast?: boolean): StreamPublisher;
createConsumer(remote: StreamRemote): StreamConsumer;
```

Events:
``` ts
'stream_added' => RemoteStream
'stream_removed' => RemoteStream
'mystream_added' => RemoteStream
'mystream_removed' => RemoteStream
```

### 2.1 StreamPublisher
In StreamPublisher we have on single method for switch or stop sharing

``` js
switchStream(stream: MediaStream | null): void;
```

### 2.2 StreamConsumer
In StreamConsumer, whe have bellow properties and methods
StreamConsumer properties
``` ts
readonly stream: MediaStream;
```

StreamConsumer methods
``` ts
view(viewer_id: string, priority?: number, min_layer?: number, max_layer?: number): MediaStream; //viewer_id is frontend generated viewer_id for this stream, this can be component id, screen id. viewer_id is used for reuse stream with multi-views
unview(viewer_id: string): void;
onStateUpdated(callback: (stream_id: string, state: string) => void): void;
onVadsUpdated(callback: (vads: {
	[key: string]: number;
}) => void): void; //key is peer_hash
```

### 2.3 RemoteStream
In RemoteStream, we have bellow properties and methods

RemoteStream Properties:
``` ts
readonly kind: StreamKinds
readonly peer_id: String
readonly peer_hash: String
readonly name: String
readonly state: StreamRemoteState = {
    simulcast: boolean,
    layers: Array,
    status: enum StreamRemoteStatus { "new", "connecting", "connected", "reconnecting", "disconnected" },
    active: boolean,
}
```

RemoteStream Events:
``` ts
'state' => StreamRemoteState,
'closed' => void,
```

## 3. Simplest echo example

``` ts
async connectStreamServer() {
    // Get media-stream
    let stream = await navigator.mediaDevices.getUserMedia({audio: true, video: true});

    // Create session with two microphone and a webcam streams. PreCreate two audio and video receiver for getting back signal
    let session = createBlueSeaSession('', {
        room_id: 'demo',
        peer_id: 'echo-client-' + new Date().getTime(),
        token: '',
        codecs: ['OPUS', 'VP8', 'VP9', 'H264'],
        senders: [
            { stream: stream, name: 'audio_main', kind: 'audio' },
            { stream: stream, name: 'video_main', prefer_codecs: ['VP8', 'VP9', 'H264'], kind: 'video', simulcast: true }
        ],
        receivers: { 
            audio: 1,
            video: 1
        }
    });
    window.bluesea_session = session;
    //register my stream added events
    session.on('mystream_added', (stream) => {
        if(stream.kind == 'audio') {
            if(window.audio_consumer) {
                window.audio_consumer.unview('main');
            }
            window.audio_consumer = await window.bluesea_session.createConsumer(stream);
            let element = document.getElementById('my_audio');
            element.srcObject = window.audio_consumer.view('main');
        }

        if(stream.kind == 'video') {
            if(window.video_consumer) {
                window.video_consumer.unview('main');
            }
            window.video_consumer = await window.bluesea_session.createConsumer(stream);
            let element = document.getElementById('my_video');
            element.srcObject = window.video_consumer.view('main');
        }
    });
    await session.connect();
}
```