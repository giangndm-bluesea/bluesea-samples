(() => {
  var __async = (__this, __arguments, generator) => {
    return new Promise((resolve, reject) => {
      var fulfilled = (value) => {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      };
      var rejected = (value) => {
        try {
          step(generator.throw(value));
        } catch (e) {
          reject(e);
        }
      };
      var step = (x) => x.done ? resolve(x.value) : Promise.resolve(x.value).then(fulfilled, rejected);
      step((generator = generator.apply(__this, __arguments)).next());
    });
  };

  // src/lib.ts
  function http_post(url, body) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    const raw = JSON.stringify(body);
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow"
    };
    return fetch(url, requestOptions).then((response) => response.text()).then((result) => JSON.parse(result));
  }
  function http_get(url) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    const requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow"
    };
    return fetch(url, requestOptions).then((response) => response.text()).then((result) => JSON.parse(result));
  }

  // src/libs/logger.ts
  function getLogger(prefix) {
    return {
      log: console.log,
      info: console.info,
      warn: console.warn,
      error: console.error
    };
  }

  // src/libs/simple-event-emitter.ts
  var EventEmitter = class {
    constructor() {
      this.events = {};
    }
    emit(event, ...args) {
      for (let i of this.events[event] || []) {
        i(...args);
      }
    }
    on(event, cb) {
      (this.events[event] = this.events[event] || []).push(cb);
      return () => this.events[event] = this.events[event].filter((i) => i !== cb);
    }
    removeAllListeners() {
      this.events = {};
    }
    removeListener(event, cb) {
      this.events[event] = this.events[event].filter((i) => i !== cb);
    }
  };

  // src/streams/data.ts
  var logger = getLogger("DataChannel");
  var DatachannelReq = class {
    constructor(req_id, method, params, resolve, reject) {
      this.req_id = req_id;
      this.method = method;
      this.params = params;
      this.resolve = resolve;
      this.reject = reject;
      this.created_at = new Date().getTime();
    }
    toJson() {
      return {
        req_id: this.req_id,
        type: "request",
        request: this.method,
        data: this.params
      };
    }
  };
  var DataChannel = class {
    constructor(channel) {
      this.channel = channel;
      this.req_seed = 0;
      this.reqs = new Map();
      this.handlers = new Map();
      this.connected = false;
      this._onReceiveMessage = (msg) => {
        logger.log("Datachannel on message:", msg.data);
        let json = JSON.parse(msg.data);
        let type = json.type;
        if (type === "event") {
          let handler = this.handlers.get(json.event);
          if (handler) {
            handler(json.event, json.data);
          }
        } else if (type === "request") {
          this.channel.send(JSON.stringify({
            type: "answer",
            status: false,
            error: "NOT_SUPPORT"
          }));
        } else if (type === "answer") {
          let req = this.reqs.get(json.req_id);
          if (!!req) {
            if (json.success === true) {
              req.resolve({
                status: true,
                data: json.data
              });
            } else {
              req.resolve({
                status: false,
                error: json.error
              });
            }
          } else {
            logger.warn("req not found for", json);
          }
        }
      };
      this.channel.onmessage = this._onReceiveMessage;
      this.channel.onopen = () => __async(this, null, function* () {
        logger.info("Datachannel opended");
        this.connected = true;
        this.handlers.get("connected")();
      });
      this.channel.onerror = (err) => {
        logger.error("Datachannel error:", err);
      };
      this.channel.onclose = () => {
        logger.info("Datachannel disconnected");
        this.connected = false;
        this.handlers.get("disconnected")();
      };
    }
    on(method, handler) {
      this.handlers.set(method, handler);
    }
    request(method, params) {
      let req_id = this.req_seed++;
      return new Promise((resolve, reject) => {
        const req = new DatachannelReq(req_id, method, params, resolve, reject);
        this.reqs.set(req_id, req);
        logger.log("send request:", req.toJson());
        this.channel.send(JSON.stringify(req.toJson()));
      });
    }
  };

  // src/streams/receiver.ts
  var logger2 = getLogger("StreamReceiver");
  var StreamReceiverState;
  (function(StreamReceiverState2) {
    StreamReceiverState2["CONNECTING"] = "connecting";
    StreamReceiverState2["LIVE"] = "live";
    StreamReceiverState2["PAUSE"] = "paused";
    StreamReceiverState2["KEY_ONLY"] = "key_only";
    StreamReceiverState2["DISCONNECTED"] = "disconnected";
  })(StreamReceiverState || (StreamReceiverState = {}));
  var _StreamReceiver = class {
    constructor(kind, remote_id) {
      this.kind = kind;
      this.remote_id = remote_id;
      this.has_track = false;
      this.has_track_checks = [];
      this.state = StreamReceiverState.DISCONNECTED;
      this.uuid = `${kind}-${_StreamReceiver.uuid_seed++}`;
      this.stream = new MediaStream();
    }
    setTrack(track) {
      this.stream.addTrack(track);
      this.has_track = true;
      this.has_track_checks.map((resolve) => {
        resolve(true);
      });
      this.has_track_checks = [];
    }
    setDatachannel(datachannel) {
      this.datachannel = datachannel;
      this.datachannel.on(`local_stream_${this.remote_id}_state`, (method, info) => {
        this.state = info.state;
        if (this._on_state_updated) {
          this._on_state_updated(this.remote_id, info.state);
        }
      });
      this.datachannel.on(`local_stream_${this.remote_id}_vad`, (method, vads) => {
        if (this._on_vads_updated) {
          this._on_vads_updated(vads);
        }
      });
    }
    onStateUpdated(callback) {
      this._on_state_updated = callback;
    }
    onVadsUpdated(callback) {
      this._on_vads_updated = callback;
    }
    internal_ready() {
      return __async(this, null, function* () {
        if (this.has_track)
          return true;
        return new Promise((resolve) => {
          this.has_track_checks.push(resolve);
        });
      });
    }
    limit(priority, max_spatial, max_temporal) {
      return __async(this, null, function* () {
        logger2.log("will switch to quality ", priority, max_spatial, max_temporal);
        yield this.internal_ready();
        if (this.datachannel && this.stream) {
          let res = yield this.datachannel.request("receiver.limit", { id: this.remote_id, limit: { priority, max_spatial, max_temporal } });
          return res.status === true;
        }
        return false;
      });
    }
    switch(remote, priority = 50) {
      return __async(this, null, function* () {
        logger2.log("will switch to view ", remote.peer_id, remote.name);
        yield this.internal_ready();
        if (this.datachannel && this.stream) {
          let res = yield this.datachannel.request("receiver.switch", { id: this.remote_id, priority, remote: { peer: remote.peer_id, stream: remote.name } });
          return res.status === true;
        }
        return false;
      });
    }
    disconnect() {
      return __async(this, null, function* () {
        yield this.internal_ready();
        if (this.datachannel && this.stream) {
          let res = yield this.datachannel.request("receiver.disconnect", { id: this.remote_id });
          return res.status === true;
        }
        return false;
      });
    }
  };
  var StreamReceiver = _StreamReceiver;
  StreamReceiver.uuid_seed = 0;

  // src/streams/remote.ts
  var StreamRemoteEvent;
  (function(StreamRemoteEvent2) {
    StreamRemoteEvent2["STATE"] = "state";
    StreamRemoteEvent2["CLOSED"] = "closed";
  })(StreamRemoteEvent || (StreamRemoteEvent = {}));
  var StreamRemoteStatus;
  (function(StreamRemoteStatus2) {
    StreamRemoteStatus2["New"] = "new";
    StreamRemoteStatus2["Connecting"] = "connecting";
    StreamRemoteStatus2["Connected"] = "connected";
    StreamRemoteStatus2["Reconnecting"] = "reconnecting";
    StreamRemoteStatus2["Disconnected"] = "disconnected";
  })(StreamRemoteStatus || (StreamRemoteStatus = {}));
  var StreamRemote = class extends EventEmitter {
    constructor(kind, peer_id, peer_hash, name) {
      super();
      this.kind = kind;
      this.peer_id = peer_id;
      this.peer_hash = peer_hash;
      this.name = name;
      this._state = {
        simulcast: false,
        layers: [],
        status: StreamRemoteStatus.New,
        active: true
      };
    }
    get state() {
      return this._state;
    }
    updateState(_state) {
      if (JSON.stringify(this._state) !== JSON.stringify(_state)) {
        this._state = _state;
        this.emit(StreamRemoteEvent.STATE, _state);
      }
    }
    onClosed() {
      this.emit(StreamRemoteEvent.CLOSED);
    }
  };

  // src/types.ts
  var StreamKinds;
  (function(StreamKinds2) {
    StreamKinds2["AUDIO"] = "audio";
    StreamKinds2["VIDEO"] = "video";
  })(StreamKinds || (StreamKinds = {}));

  // src/streams/sender.ts
  var StreamSender = class {
    constructor(peer_id, kind, name, stream, simulcast, session, prefer_codec, max_bitrate, raw_sender) {
      this.peer_id = peer_id;
      this.kind = kind;
      this.name = name;
      this.simulcast = simulcast;
      this.session = session;
      this.prefer_codec = prefer_codec;
      this.max_bitrate = max_bitrate;
      this.raw_sender = raw_sender;
      var _a, _b;
      this.stream = stream;
      this._uuid = ((_a = this.track()) == null ? void 0 : _a.id) || "not-supported";
      this._label = ((_b = this.track()) == null ? void 0 : _b.label) || "not-supported";
    }
    setRawSender(transceiver, raw_sender) {
      if (!!this.prefer_codec) {
        let codecs = RTCRtpSender.getCapabilities(this.kind).codecs;
        codecs.sort((c1, c2) => {
          let c1_index = this.prefer_codec.indexOf(c1.mimeType.replace("video/", ""));
          let c2_index = this.prefer_codec.indexOf(c2.mimeType.replace("video/", ""));
          if (c1_index < 0)
            c1_index = 1e3;
          if (c2_index < 0)
            c2_index = 1e3;
          if (c1_index < c2_index) {
            return -1;
          }
          if (c1_index > c2_index) {
            return 1;
          }
          return 0;
        });
        transceiver.setCodecPreferences(codecs);
      }
      this.raw_sender = raw_sender;
    }
    setDatachannel(datachannel) {
      this.datachannel = datachannel;
    }
    get Uuid() {
      return this._uuid;
    }
    get Label() {
      return this._label;
    }
    setActive(active) {
      return __async(this, null, function* () {
        return this.datachannel.request("sender.set_active_other", { peer: this.peer_id, name: this.name, kind: this.kind, active });
      });
    }
    stop() {
      return __async(this, null, function* () {
        yield this.session.deleteSender(this, this.raw_sender);
      });
    }
    replaceStream(stream) {
      return __async(this, null, function* () {
        this.stream = stream;
        if (!!this.raw_sender) {
          let track = this.track();
          this.datachannel.request("sender.toggle", { name: this.name, kind: this.kind, track: track == null ? void 0 : track.id });
          this.raw_sender.replaceTrack(track);
        }
      });
    }
    toggleAudioFeatures(vad, remove_noise) {
      return __async(this, null, function* () {
        if (this.kind !== StreamKinds.AUDIO)
          throw new Error("OnlySupportAudioSender");
        yield this.datachannel.request("audio.toggle_features", { name: this.name, vad, remove_noise });
      });
    }
    sending() {
      if (!!this.raw_sender) {
        return !!this.raw_sender.track;
      }
      return false;
    }
    track() {
      if (!this.stream)
        return null;
      switch (this.kind) {
        case StreamKinds.AUDIO:
          return this.stream.getAudioTracks()[0];
        case StreamKinds.VIDEO:
          return this.stream.getVideoTracks()[0];
        default:
          return null;
      }
    }
  };

  // node_modules/ts-debounce/dist/src/index.esm.js
  function r(r2, e, n) {
    var i, t, o;
    e === void 0 && (e = 50), n === void 0 && (n = {});
    var a = (i = n.isImmediate) != null && i, u = (t = n.callback) != null && t, c = n.maxWait, v = Date.now(), l = [];
    function f() {
      if (c !== void 0) {
        var r3 = Date.now() - v;
        if (r3 + e >= c)
          return c - r3;
      }
      return e;
    }
    var d = function() {
      var e2 = [].slice.call(arguments), n2 = this;
      return new Promise(function(i2, t2) {
        var c2 = a && o === void 0;
        if (o !== void 0 && clearTimeout(o), o = setTimeout(function() {
          if (o = void 0, v = Date.now(), !a) {
            var i3 = r2.apply(n2, e2);
            u && u(i3), l.forEach(function(r3) {
              return (0, r3.resolve)(i3);
            }), l = [];
          }
        }, f()), c2) {
          var d2 = r2.apply(n2, e2);
          return u && u(d2), i2(d2);
        }
        l.push({ resolve: i2, reject: t2 });
      });
    };
    return d.cancel = function(r3) {
      o !== void 0 && clearTimeout(o), l.forEach(function(e2) {
        return (0, e2.reject)(r3);
      }), l = [];
    }, d;
  }

  // src/streams/consumer.ts
  var StreamConsumer = class {
    constructor(_stream, session) {
      this._stream = _stream;
      this.session = session;
      this.views = new Map();
    }
    get stream() {
      var _a;
      return (_a = this.receiver) == null ? void 0 : _a.stream;
    }
    view(viewer_id, priority = 50, min_layer = 0, max_layer = 2) {
      this.views.set(viewer_id, { priority, min_layer, max_layer });
      if (!this.receiver) {
        this.receiver = this.session.takeReceiver(this._stream.kind);
        this.receiver.onStateUpdated(this._on_state_updated);
        this.receiver.onVadsUpdated(this._on_vads_updated);
        this.receiver.switch(this._stream, priority);
      }
      this.configLayer();
      return this.receiver.stream;
    }
    unview(viewer_id) {
      this.views.delete(viewer_id);
      if (this.views.size == 0) {
        if (!!this.receiver) {
          this.receiver.disconnect();
          this.receiver.onStateUpdated(null);
          this.receiver.onVadsUpdated(null);
          this.session.backReceiver(this.receiver);
          this.receiver = void 0;
        }
      } else {
        this.configLayer();
      }
    }
    onStateUpdated(callback) {
      this._on_state_updated = callback;
      if (!!this.receiver)
        this.receiver.onStateUpdated(this._on_state_updated);
    }
    onVadsUpdated(callback) {
      this._on_vads_updated = callback;
      if (!!this.receiver)
        this.receiver.onVadsUpdated(this._on_vads_updated);
    }
    configLayer() {
      if (this._stream.kind !== StreamKinds.VIDEO)
        return;
      let selected_priority = 0;
      let selected_min_layer = 0;
      let selected_max_layer = 2;
      Array.from(this.views.values()).map((viewer) => {
        selected_priority = Math.max(selected_priority, viewer.priority);
        selected_min_layer = Math.min(selected_min_layer, viewer.min_layer);
        selected_max_layer = Math.max(selected_max_layer, viewer.max_layer);
      });
      this.receiver.limit(selected_priority, selected_min_layer, selected_max_layer);
    }
  };

  // src/streams/publisher.ts
  var StreamPublisher = class {
    constructor(kind, name, simulcast, session) {
      this.kind = kind;
      this.name = name;
      this.simulcast = simulcast;
      this.session = session;
      this.sender = this.session.getSender(kind, name);
    }
    switchStream(stream) {
      if (!!this.sender) {
        this.sender.replaceStream(stream);
      } else if (stream != null) {
        this.sender = this.session.createSender(this.kind, this.name, stream, this.simulcast);
      }
    }
  };

  // src/streams/planb_transceiver.ts
  var PlanBTransceiverSender = class {
    constructor(track) {
      this.track = track;
    }
    getParameters() {
      return {};
    }
    getStats() {
      return __async(this, null, function* () {
        return {};
      });
    }
    replaceTrack(withTrack) {
      return __async(this, null, function* () {
        if (withTrack == null) {
          this.track.enabled = false;
        } else {
          this.track.enabled = true;
        }
      });
    }
    switchCamera() {
      this.track._switchCamera();
    }
    setParameters(parameters) {
      return __async(this, null, function* () {
      });
    }
    setStreams(...streams) {
    }
  };
  var PlanBTransceiver = class {
    constructor(sender_track) {
      this.sender_track = sender_track;
      if (sender_track) {
        this.sender = new PlanBTransceiverSender(sender_track);
      }
    }
    setCodecPreferences(codecs) {
    }
    stop() {
    }
  };

  // src/session.ts
  var logger3 = getLogger("BlueSeaSession");
  var BlueSeaCodec;
  (function(BlueSeaCodec2) {
    BlueSeaCodec2["OPUS"] = "OPUS";
    BlueSeaCodec2["VP8"] = "VP8";
    BlueSeaCodec2["VP9"] = "VP9";
    BlueSeaCodec2["H264"] = "H264";
  })(BlueSeaCodec || (BlueSeaCodec = {}));
  var BlueSeaSessionEvent;
  (function(BlueSeaSessionEvent2) {
    BlueSeaSessionEvent2["CONNECTED"] = "connected";
    BlueSeaSessionEvent2["DISCONNECTED"] = "disconnected";
    BlueSeaSessionEvent2["STREAM_ADDED"] = "stream_added";
    BlueSeaSessionEvent2["STREAM_REMOVED"] = "stream_removed";
    BlueSeaSessionEvent2["STREAM_UPDATED"] = "stream_updated";
    BlueSeaSessionEvent2["MY_STREAM_ADDED"] = "mystream_added";
    BlueSeaSessionEvent2["MY_STREAM_REMOVED"] = "mystream_removed";
    BlueSeaSessionEvent2["MY_STREAM_UPDATED"] = "mystream_updated";
  })(BlueSeaSessionEvent || (BlueSeaSessionEvent = {}));
  var BlueSeaSession2 = class extends EventEmitter {
    constructor(url, config) {
      super();
      this.url = url;
      this.config = config;
      this.audio_senders = new Map();
      this.video_senders = new Map();
      this.audio_receivers = [];
      this.video_receivers = [];
      this.free_audio_receivers = [];
      this.free_video_receivers = [];
      this.remote_streams = new Map();
      this.updateSdp = r(this.updateSdpRaw, 500, {
        isImmediate: false
      });
      this.onDcConnected = () => {
        this.audio_receivers.map((r2) => r2.setDatachannel(this.dataChannel));
        this.video_receivers.map((r2) => r2.setDatachannel(this.dataChannel));
        this.emit(BlueSeaSessionEvent.CONNECTED);
      };
      this.onDcDisconnected = () => {
      };
      this.onNewTrack = (event) => {
        const streams = event.streams;
        const track = event.track;
        if (streams.length == 0) {
          logger3.warn("invalid track without streams:", track, streams);
          return;
        }
        if (track.kind == StreamKinds.AUDIO) {
          for (let i = 0; i < this.audio_receivers.length; i++) {
            let receiver = this.audio_receivers[i];
            if (receiver.stream.getTracks().length == 0 && receiver.kind === track.kind && streams[0].id == receiver.remote_id) {
              receiver.setTrack(track);
              logger3.info("found audio receiver for stream:", receiver.remote_id, track, receiver, streams[0]);
              return;
            }
          }
        } else if (track.kind == StreamKinds.VIDEO) {
          for (let i = 0; i < this.video_receivers.length; i++) {
            let receiver = this.video_receivers[i];
            if (receiver.stream.getTracks().length == 0 && receiver.kind === track.kind && streams[0].id == receiver.remote_id) {
              receiver.setTrack(track);
              logger3.info("found video receiver for stream:", receiver.remote_id, track, receiver, streams[0]);
              return;
            }
          }
        }
        logger3.warn("Not found receiver for stream:", track, this.audio_receivers, this.video_receivers, streams);
      };
      this.onStreamEvents = (event, params) => {
        logger3.info("on stream event:", event, params);
        let key = `${params.peer}/${params.stream}`;
        let is_me = this.config.peer_id;
        switch (event) {
          case "stream_added": {
            let old_stream = this.remote_streams.get(key);
            if (!old_stream) {
              let stream = new StreamRemote(params.kind, params.peer, params.peer_hash, params.stream);
              stream.updateState(params.state);
              if (params.peer === this.config.peer_id) {
                this.emit(BlueSeaSessionEvent.MY_STREAM_ADDED, stream);
              } else {
                this.emit(BlueSeaSessionEvent.STREAM_ADDED, stream);
              }
              this.remote_streams.set(key, stream);
            } else {
              old_stream.updateState(params.state);
              if (params.peer === this.config.peer_id) {
                this.emit(BlueSeaSessionEvent.MY_STREAM_UPDATED, old_stream);
              } else {
                this.emit(BlueSeaSessionEvent.STREAM_UPDATED, old_stream);
              }
            }
            break;
          }
          case "stream_updated": {
            let old_stream = this.remote_streams.get(key);
            if (!old_stream) {
              let stream = new StreamRemote(params.kind, params.peer, params.peer_hash, params.stream);
              stream.updateState(params.state);
              if (params.peer === this.config.peer_id) {
                this.emit(BlueSeaSessionEvent.MY_STREAM_ADDED, stream);
              } else {
                this.emit(BlueSeaSessionEvent.STREAM_ADDED, stream);
              }
              this.remote_streams.set(key, stream);
            } else {
              old_stream.updateState(params.state);
              if (params.peer === this.config.peer_id) {
                this.emit(BlueSeaSessionEvent.MY_STREAM_UPDATED, old_stream);
              } else {
                this.emit(BlueSeaSessionEvent.STREAM_UPDATED, old_stream);
              }
            }
            break;
          }
          case "stream_removed": {
            let old_stream = this.remote_streams.get(key);
            if (old_stream) {
              old_stream.onClosed();
              this.remote_streams.delete(key);
              if (params.peer === this.config.peer_id) {
                this.emit(BlueSeaSessionEvent.MY_STREAM_REMOVED, old_stream);
              } else {
                this.emit(BlueSeaSessionEvent.STREAM_REMOVED, old_stream);
              }
            }
            break;
          }
        }
      };
      logger3.info("Created session:", url);
      config.senders.filter((s) => s.kind === StreamKinds.AUDIO).map((s) => {
        let sender = new StreamSender(config.peer_id, s.kind, s.name, s.stream, false, this, s.prefer_codecs);
        this.audio_senders.set(s.name, sender);
      });
      config.senders.filter((s) => s.kind === StreamKinds.VIDEO).map((s) => {
        let sender = new StreamSender(config.peer_id, s.kind, s.name, s.stream, !!s.simulcast, this, s.prefer_codecs, s.max_bitrate);
        this.video_senders.set(s.name, sender);
      });
      for (let i = 0; i < config.receivers.audio; i++) {
        let receiver = new StreamReceiver(StreamKinds.AUDIO, `audio_${i}`);
        this.audio_receivers.push(receiver);
        this.free_audio_receivers.push(receiver);
      }
      for (let i = 0; i < config.receivers.video; i++) {
        let receiver = new StreamReceiver(StreamKinds.VIDEO, `video_${i}`);
        this.video_receivers.push(receiver);
        this.free_video_receivers.push(receiver);
      }
      if (window.addEventListener) {
        window.addEventListener("beforeunload", (e) => {
          var _a;
          (_a = this.peer) == null ? void 0 : _a.close();
        });
      }
    }
    select_url() {
      return __async(this, null, function* () {
        return new Promise((resolve, reject) => {
          if (typeof this.url === "string") {
            resolve(this.url);
          } else {
            let waiting_urls = [];
            let finished = false;
            this.url.map((url) => __async(this, null, function* () {
              console.log("checking ", url);
              waiting_urls[url] = true;
              try {
                let res = yield http_get(url + "/healthcheck?ts=" + new Date().getTime());
                console.log("on res:", res);
                if (finished)
                  return;
                if (res.status === true && res.data && res.data.ready === true) {
                  finished = true;
                  return resolve(url);
                } else {
                  console.log("on error:", url, res);
                  delete waiting_urls[url];
                  if (Object.keys(waiting_urls).length == 0) {
                    reject(new Error("NotNotReady"));
                  }
                }
              } catch (err) {
                if (finished)
                  return;
                delete waiting_urls[url];
                console.error("on error:", waiting_urls, url, err);
                if (Object.keys(waiting_urls).length == 0) {
                  reject(new Error("NotNotReady"));
                }
              }
            }));
          }
        });
      });
    }
    connect() {
      return __async(this, null, function* () {
        let url = yield this.select_url();
        logger3.info("connect to ", url);
        this.peer = new RTCPeerConnection();
        this.dataChannel = new DataChannel(this.peer.createDataChannel("data", {
          ordered: false,
          maxPacketLifeTime: 1e4
        }));
        if (this.peer.addTransceiver) {
          this.peer.ontrack = this.onNewTrack;
        } else {
          this.peer.addEventListener("addstream", (event) => {
            this.onNewTrack({ streams: [event.stream], track: event.stream.getTracks()[0] });
          });
        }
        this.peer.onconnectionstatechange = () => {
          logger3.info("state:", this.peer.connectionState);
          switch (this.peer.connectionState) {
            case "connected":
              break;
            case "disconnected":
            case "failed":
              this.emit(BlueSeaSessionEvent.DISCONNECTED);
              break;
            case "closed":
              this.emit(BlueSeaSessionEvent.DISCONNECTED);
              break;
          }
        };
        yield Promise.all(Array.from(this.audio_senders.values()).map((s) => __async(this, null, function* () {
          logger3.info("create audio pre_sender:", s.track().kind, s.track().id);
          if (this.peer.addTransceiver) {
            let transceiver = this.peer.addTransceiver(s.track(), {
              direction: "sendonly",
              streams: [s.stream]
            });
            s.setRawSender(transceiver, transceiver.sender);
          } else {
            this.peer.addStream(s.stream);
            let transceiver = new PlanBTransceiver(s.track());
            s.setRawSender(transceiver, transceiver.sender);
          }
          s.setDatachannel(this.dataChannel);
        })));
        yield Promise.all(Array.from(this.video_senders.values()).map((s) => __async(this, null, function* () {
          logger3.info("create video pre_sender:", s.track().kind, s.track().id);
          if (this.peer.addTransceiver) {
            let transceiver = null;
            if (s.simulcast) {
              transceiver = this.peer.addTransceiver(s.track(), {
                direction: "sendonly",
                streams: [s.stream],
                sendEncodings: [
                  { rid: "1", active: true, maxBitrate: s.max_bitrate ? Math.floor(s.max_bitrate * 4 / 5) : 5e5, scaleResolutionDownBy: 1 },
                  { rid: "0", active: true, maxBitrate: s.max_bitrate ? Math.floor(s.max_bitrate * 1 / 5) : 1e5, scaleResolutionDownBy: 2 }
                ]
              });
              s.setRawSender(transceiver, transceiver.sender);
            } else {
              transceiver = this.peer.addTransceiver(s.track(), {
                direction: "sendonly",
                streams: [s.stream],
                sendEncodings: s.max_bitrate ? [{ maxBitrate: s.max_bitrate }] : void 0
              });
              s.setRawSender(transceiver, transceiver.sender);
            }
          } else {
            this.peer.addStream(s.stream);
            let transceiver = new PlanBTransceiver(s.track());
            s.setRawSender(transceiver, transceiver.sender);
          }
          s.setDatachannel(this.dataChannel);
        })));
        this.audio_receivers.map((r2) => {
          logger3.info("create pre_receiver:", StreamKinds.AUDIO);
          if (this.peer.addTransceiver) {
            this.peer.addTransceiver(StreamKinds.AUDIO, {
              direction: "recvonly"
            });
          }
        });
        this.video_receivers.map((r2) => {
          logger3.info("create pre_receiver:", StreamKinds.VIDEO);
          if (this.peer.addTransceiver) {
            let trans = this.peer.addTransceiver(StreamKinds.VIDEO, {
              direction: "recvonly"
            });
            this.settingTransiverCodecs(trans);
          }
        });
        this.dataChannel.on("connected", this.onDcConnected);
        this.dataChannel.on("disconnected", this.onDcDisconnected);
        this.dataChannel.on("stream_added", this.onStreamEvents);
        this.dataChannel.on("stream_updated", this.onStreamEvents);
        this.dataChannel.on("stream_removed", this.onStreamEvents);
        const offer = yield this.peer.createOffer({
          offerToReceiveAudio: true,
          offerToReceiveVideo: true
        });
        logger3.info("created offer:", offer.sdp);
        let room_id = this.config.room_id;
        let peer_id = this.config.peer_id;
        let token = this.config.token;
        let custom_query = this.config.custom_query;
        const res = yield http_post(url + `/webrtc/connect?room=${room_id}&peer=${peer_id}&token=${token}${custom_query ? `&${custom_query}` : ""}`, {
          sdp: offer.sdp,
          codecs: this.config.codecs,
          senders: Array.from(this.audio_senders.values()).map((s) => {
            return { uuid: s.Uuid, label: s.Label, kind: s.kind, name: s.name };
          }).concat(Array.from(this.video_senders.values()).map((s) => {
            return { uuid: s.Uuid, label: s.Label, kind: s.kind, name: s.name };
          })),
          receivers: {
            audio: this.audio_receivers.length,
            video: this.video_receivers.length
          }
        });
        if (!res.status) {
          logger3.error("Error response form server", res);
          throw new Error(res.error_code);
        }
        let node_id = res.data.node_id;
        let conn_id = res.data.conn_id;
        let sdp = res.data.node_id;
        this.node_id = node_id;
        this.conn_id = conn_id;
        logger3.info("received answer:", node_id, conn_id, sdp);
        this.peer.onicecandidate = (ice) => {
          if (ice && ice.candidate) {
            http_post(url + `/webrtc/ice_remote?node_id=${node_id}&conn_id=${conn_id}${custom_query ? `&${custom_query}` : ""}`, {
              candidate: ice.candidate.candidate || "",
              sdp_mid: ice.candidate.sdpMid || "",
              sdp_mline_index: ice.candidate.sdpMLineIndex || 0,
              username_fragment: ice.candidate.usernameFragment || ""
            });
          } else {
          }
        };
        this.peer.setLocalDescription(offer);
        this.peer.setRemoteDescription(new RTCSessionDescription({
          type: "answer",
          sdp: res.data.sdp
        }));
      });
    }
    disconnect() {
      return __async(this, null, function* () {
        var _a, _b;
        yield (_a = this.dataChannel) == null ? void 0 : _a.request("peer.close", {});
        (_b = this.peer) == null ? void 0 : _b.close();
      });
    }
    ready() {
      return new Promise((resolve, reject) => {
        if (this.dataChannel && this.dataChannel.connected)
          return resolve(true);
        let interval = setInterval(() => {
          if (this.dataChannel && this.dataChannel.connected) {
            resolve(true);
            clearInterval(interval);
          }
        }, 100);
      });
    }
    updateSdpRaw() {
      return __async(this, null, function* () {
        const offer = yield this.peer.createOffer({
          offerToReceiveAudio: true,
          offerToReceiveVideo: true
        });
        const meta = {
          sdp: offer.sdp,
          senders: Array.from(this.audio_senders.values()).map((s) => {
            return { uuid: s.Uuid, label: s.Label, kind: s.kind, name: s.name };
          }).concat(Array.from(this.video_senders.values()).map((s) => {
            return { uuid: s.Uuid, label: s.Label, kind: s.kind, name: s.name };
          })),
          receivers: {
            audio: this.audio_receivers.length,
            video: this.video_receivers.length
          }
        };
        logger3.info("send updated sdp:", meta);
        let res = yield this.dataChannel.request("peer.updateSdp", meta);
        if (!res.status) {
          logger3.error("Error response form server", res);
          throw new Error("SERVER_ERROR");
        }
        logger3.info("received answer:", res.data);
        this.peer.setLocalDescription(offer);
        this.peer.setRemoteDescription(new RTCSessionDescription({
          type: "answer",
          sdp: res.data.sdp
        }));
      });
    }
    createPublisher(kind, name, simulcast = false) {
      return new StreamPublisher(kind, name, simulcast, this);
    }
    createConsumer(remote) {
      return new StreamConsumer(remote, this);
    }
    createSender(type, name, stream, simulcast, prefer_codecs, max_bitrate) {
      const sender = new StreamSender(this.config.peer_id, type, name, stream, !!simulcast, this, prefer_codecs);
      if (this.peer.addTransceiver) {
        if (!!simulcast) {
          let transceiver = this.peer.addTransceiver(sender.track(), {
            direction: "sendonly",
            streams: [sender.stream],
            sendEncodings: [
              { rid: "1", active: true, maxBitrate: max_bitrate ? Math.floor(max_bitrate * 4 / 5) : 5e5, scaleResolutionDownBy: 1 },
              { rid: "0", active: true, maxBitrate: max_bitrate ? Math.floor(max_bitrate * 1 / 5) : 1e5, scaleResolutionDownBy: 2 }
            ]
          });
          sender.setRawSender(transceiver, transceiver.sender);
        } else {
          let transceiver = this.peer.addTransceiver(sender.track(), {
            direction: "sendonly",
            streams: [sender.stream],
            sendEncodings: max_bitrate ? [{ maxBitrate: max_bitrate }] : void 0
          });
          sender.setRawSender(transceiver, transceiver.sender);
        }
      } else {
        this.peer.addStream(stream);
        let transceiver = new PlanBTransceiver(sender.track());
        sender.setRawSender(transceiver, transceiver.sender);
      }
      sender.setDatachannel(this.dataChannel);
      switch (type) {
        case StreamKinds.AUDIO:
          this.audio_senders.set(name, sender);
          break;
        case StreamKinds.VIDEO:
          this.video_senders.set(name, sender);
          break;
        default:
          break;
      }
      this.updateSdp();
      return sender;
    }
    deleteSender(sender, raw_sender) {
      if (!!raw_sender) {
        logger3.info("delete sender:", raw_sender);
        this.peer.removeTrack(raw_sender);
      }
      switch (sender.kind) {
        case StreamKinds.AUDIO:
          this.audio_senders.delete(sender.name);
          break;
        case StreamKinds.VIDEO:
          this.video_senders.delete(sender.name);
          break;
        default:
          break;
      }
      this.updateSdp();
    }
    takeReceiver(type) {
      logger3.info("take receiver to queue", type);
      if (type == StreamKinds.AUDIO) {
        if (this.free_audio_receivers.length == 0) {
          this.createReceiver(StreamKinds.AUDIO);
        }
        let receiver = this.free_audio_receivers[0];
        this.free_audio_receivers.shift();
        return receiver;
      } else if (type == StreamKinds.VIDEO) {
        if (this.free_video_receivers.length == 0) {
          this.createReceiver(StreamKinds.VIDEO);
        }
        let receiver = this.free_video_receivers[0];
        this.free_video_receivers.shift();
        return receiver;
      }
      throw new Error("UNSUPPORTED_TYPE");
    }
    settingTransiverCodecs(trans) {
      if (this.config.codecs) {
        let codecs = RTCRtpReceiver.getCapabilities(StreamKinds.VIDEO).codecs;
        codecs.sort((c1, c2) => {
          let c1_index = this.config.codecs.indexOf(c1.mimeType.replace("video/", ""));
          let c2_index = this.config.codecs.indexOf(c2.mimeType.replace("video/", ""));
          if (c1_index < 0)
            c1_index = 1e3;
          if (c2_index < 0)
            c2_index = 1e3;
          if (c1_index < c2_index) {
            return -1;
          }
          if (c1_index > c2_index) {
            return 1;
          }
          return 0;
        });
        console.log("setting codecs:", codecs);
        trans.setCodecPreferences(codecs);
      }
    }
    createReceiver(kind) {
      if (this.peer.addTransceiver) {
        let trans = this.peer.addTransceiver(kind, {
          direction: "recvonly"
        });
        if (kind === StreamKinds.VIDEO) {
          this.settingTransiverCodecs(trans);
        }
      }
      if (kind == StreamKinds.AUDIO) {
        let receiver = new StreamReceiver(kind, `${kind}_${this.audio_receivers.length}`);
        receiver.setDatachannel(this.dataChannel);
        this.audio_receivers.push(receiver);
        this.free_audio_receivers.push(receiver);
      } else if (kind == StreamKinds.VIDEO) {
        let receiver = new StreamReceiver(kind, `${kind}_${this.video_receivers.length}`);
        receiver.setDatachannel(this.dataChannel);
        this.video_receivers.push(receiver);
        this.free_video_receivers.push(receiver);
      }
      this.updateSdp();
    }
    backReceiver(receiver) {
      logger3.info("back receiver to queue", receiver.kind);
      if (receiver.kind == StreamKinds.AUDIO) {
        this.free_audio_receivers.push(receiver);
      } else if (receiver.kind == StreamKinds.VIDEO) {
        this.free_video_receivers.push(receiver);
      }
      return true;
    }
    getSender(kind, name) {
      switch (kind) {
        case StreamKinds.AUDIO:
          return this.audio_senders.get(name);
        case StreamKinds.VIDEO:
          return this.video_senders.get(name);
        default:
          return void 0;
      }
    }
    setActiveOther(peer, kind, name, active) {
      return __async(this, null, function* () {
        var _a;
        return (_a = this.dataChannel) == null ? void 0 : _a.request("sender.set_active_other", { peer, name, kind, active });
      });
    }
    moveGroup(group) {
      return __async(this, null, function* () {
        var _a;
        yield (_a = this.dataChannel) == null ? void 0 : _a.request("group.move", { group });
      });
    }
    moveGroupMany(requests) {
      return __async(this, null, function* () {
        var _a;
        yield (_a = this.dataChannel) == null ? void 0 : _a.request("group.move_main", { pairs: requests });
      });
    }
    resetGroups() {
      return __async(this, null, function* () {
        var _a;
        yield (_a = this.dataChannel) == null ? void 0 : _a.request("group.reset", {});
      });
    }
  };

  // src/index.ts
  function createBlueSeaSession(url, config) {
    return new BlueSeaSession2(url, config);
  }
  window.createBlueSeaSession = createBlueSeaSession;
})();
//# sourceMappingURL=bluesea.sdk.js.map
