async function onStreamAdded(stream) {
    if(stream.kind == 'audio') {
        let element = document.getElementById('remote_audio');
        if(element.consumer) {
            element.consumer.unview('main');
        }
        let consumer = window.bluesea_session.createConsumer(stream);
        element.srcObject = consumer.view('main');
        element.consumer = consumer;
    }

    if(stream.kind == 'video') {
        let element = document.getElementById('remote_video');
        if(element.consumer) {
            element.consumer.unview('main');
        }
        let consumer = window.bluesea_session.createConsumer(stream);
        element.srcObject = consumer.view('main');
        element.consumer = consumer;
    }
}

async function onStreamRemoved(stream) {
    if(stream.kind == 'audio') {
        let element = document.getElementById('remote_audio');
        if(element.consumer) {
            element.consumer.unview('main');
        }
    }

    if(stream.kind == 'video') {
        let element = document.getElementById('remote_video');
        if(element.consumer) {
            element.consumer.unview('main');
        }
    }
}

async function boot() {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    let session = createBlueSeaSession(['MEDIA_SERVER'], {
        room_id: params['room'] || 'demo-tk',
        peer_id: params['peer'] || 'echo-client-' + new Date().getTime(),
        token: params['token'],
        senders: [],
        receivers: { 
            audio: 1,
            video: 1
        }
    });
    window.bluesea_session = session;
    window.audio_publisher = window.bluesea_session.createPublisher('audio', 'audio_main');
    window.video_publisher = window.bluesea_session.createPublisher('video', 'video_main', true);
    session.connect();
    session.on('stream_added', onStreamAdded);
    session.on('stream_removed', onStreamRemoved);
    session.on('disconnected', () => {
        session.removeAllListeners();
        console.log('reconnect now');
        boot();
    });
}

async function share_audio_video() {
    let stream = await navigator.mediaDevices.getUserMedia({audio: true, video: {
        frameRate: { ideal: 24 },
        width: { ideal: 4096 }
    }});
    await window.audio_publisher.switchStream(stream);
    await window.video_publisher.switchStream(stream);
}

async function unshare_audio_video() {
    await window.audio_publisher.switchStream(null);
    await window.video_publisher.switchStream(null);
}

boot();