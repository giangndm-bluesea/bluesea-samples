async function onMyStreamAdded(stream) {
    if(stream.kind == 'video') {
        let element = document.getElementById('my_video');
        if(element.consumer) {
            element.consumer.unview('main');
        }
        let consumer = window.bluesea_session.createConsumer(stream);
        element.srcObject = consumer.view('main');
        element.consumer = consumer;
    }
}

async function boot() {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());

    let stream = await navigator.mediaDevices.getUserMedia({audio: true, video: true});
    let session = createBlueSeaSession(['MEDIA_SERVER'], {
        room_id: params['room'] || 'demo-tk',
        peer_id: params['peer'] || 'echo-client-' + new Date().getTime(),
        token: params['token'],
        codecs: ['OPUS', 'VP9', 'VP8', 'H264'],
        custom_query: params['custom_query'],
        senders: [
            { stream: stream, name: 'video_main', kind: 'video', prefer_codecs: ['VP9', 'VP8', 'H264'], simulcast: true },
            { stream: stream, name: 'audio_main', kind: 'audio' }
        ],
        receivers: { 
            audio: 1,
            video: 1
        }
    });
    window.bluesea_session = session;
    session.on('mystream_added', onMyStreamAdded);
    session.on('disconnected', () => {
        session.removeAllListeners();
        console.log('reconnect now');
        boot();
    });
    await session.connect();
    window.audio_publisher = window.bluesea_session.createPublisher('audio', 'audio_main');
    window.video_publisher = window.bluesea_session.createPublisher('video', 'video_main', false);
}

boot();