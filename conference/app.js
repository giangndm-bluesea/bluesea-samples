async function onStreamAdded(stream) {
    console.log('added stream:', stream);
    if(stream.kind == 'video') {
        console.log('create video consumer');
        let consumer = window.bluesea_session.createConsumer(stream);
        let element = document.createElement('video');
        element.id = 'video-' + stream.peer_id;
        element.width = 300;
        element.height = 200;
        element.muted = true;
        element.autoplay = true;
        element.srcObject = consumer.view('main');
        element.consumer = consumer;
        document.body.appendChild(element);
    }

    if(stream.kind == 'audio' && stream.peer_id == 'mixer' && stream.name == 'audio_main') {
        console.log('create audio mixer consumer');
        let consumer = await window.bluesea_session.createConsumer(stream);
        let element = document.createElement('audio');
        element.id = 'audio-' + stream.peer_id;
        element.hidden = true;
        element.autoplay = true;
        element.srcObject = consumer.view('main');
        element.consumer = consumer;
        document.body.appendChild(element);
    }
}

async function onStreamRemoved(stream) {
    console.log('removed stream:', stream);
    if(stream.kind == 'video') {
        let element = document.getElementById('video-' + stream.peer_id);
        element.consumer.unview('main');
        element.remove();
    } else if(stream.kind == 'audio' && stream.peer_id == 'mixer' && stream.name == 'audio_main') {
        let element = document.getElementById('audio-' + stream.peer_id);
        element.consumer.unview('main');
        element.remove();
    }
}

async function boot() {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());

    let stream = await navigator.mediaDevices.getUserMedia({audio: true, video: {
        width: { ideal: 1500 },
        height: { ideal: 1000 }
    }});
    let session = createBlueSeaSession(['MEDIA_SERVER'], {
        room_id: params['room'] || 'demo-tk',
        peer_id: params['peer'] || 'echo-client-' + new Date().getTime(),
        token: params['token'],
        custom_query: params['custom_query'],
        codecs: ['OPUS', 'VP9', 'VP9', 'H264'],
        senders: [
            { stream: stream, name: 'audio_main', kind: 'audio' },
            { stream: stream, name: 'video_main', prefer_codecs: ['VP9', 'VP8', 'H264'], kind: 'video', simulcast: true, max_bitrate: 2500000 }
        ],
        receivers: { 
            audio: 1,
            video: 5
        }
    });
    window.bluesea_session = session;
    session.connect().then(() => {
        console.log('Connected');
    }).catch((err) => {
        console.log('Connect error', err);
    })
    session.on('stream_added', onStreamAdded);
    session.on('stream_removed', onStreamRemoved);
}

boot();
